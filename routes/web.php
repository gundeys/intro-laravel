<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@daftar');
Route::post('/welcome', 'AuthController@datang');

Route::get('/data-tables', function(){
    return view ('halaman.data-tables');
});

Route::get('/table', function(){
    return view ('halaman.table');
});

//CRUD

//CREATE
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');

//READ
Route::get('/cast', 'CastController@index');

Route::get('/cast/{cast_id}', 'CastController@show');

//UPDATE
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');

//DESTROY
Route::delete('/cast/{cast_id}', 'CastController@destroy');