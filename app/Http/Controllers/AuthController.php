<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view ('halaman.daftar');
    }

    public function datang(Request $request)
    {
        $first = $request['depan'];
        $last = $request['belakang'];

        return view('halaman.selamatdatang', compact('first', 'last'));
    }
}
