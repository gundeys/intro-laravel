@extends('layout.master')

@section('judul')
Halaman Form
@endsection
@section('content')
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name:</label><br><br>
        <input type="text" name="depan"><br><br>
        <label>Last Name:</label><br><br>
        <input type="text" name="belakang"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender">Man<br><br>
        <input type="radio" name="gender">Woman<br><br>
        <input type="radio" name="gender">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="national">
            <option value="indonesia">Indonesian</option>
            <option value="singapore">Singaporean</option>
            <option value="malaysia">Malaysian</option>
            <option value="australia">Australian</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bahasa">Bahasa Indonesia<br><br>
        <input type="checkbox" name="bahasa">English<br><br>
        <input type="checkbox" name="bahasa">Arabic<br><br>
        <input type="checkbox" name="bahasa">Japanese<br><br>
        <label>Bio:</label><br><br>
        <textarea name="alamat" id="" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
    @endsection
